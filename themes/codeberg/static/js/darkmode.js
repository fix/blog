function toggleDarkMode(){
  halfmoon.toggleDarkMode();

  var fa = document.getElementsByClassName("fa");
  for (var i = 0; i < fa.length; i++) {
    if (fa[i].classList.contains('fa-moon') || fa[i].classList.contains('fa-sun')){
      fa[i].classList.toggle('fa-sun');
      fa[i].classList.toggle('fa-moon');
    }
  }
}