Title: Monthly Report February 2019
Date: 2019-03-06
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down of the monthly news letter sent out to members of Codeberg e.V one week ago)

Dear Codeberg e.V. Members and Supporters!

February has been the first "regular" month after the excitement of our launch in January. It is time for some updates.

We are hosting 413 repositories, created and maintained by 421 users. Compared to one month ago, this is an organic growth of rate of +80 repositories (+24% month-over-month), and +42 users (+11%).

The machines are runnnig at less than 25% capacity, we will scale up as soon we come close to the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

Where are we going next? We still have some pending items on the run-up TODO list, these include working out the last remaining issues after migration (hopefully done next days), and improving Gitea to suit all Codeberg.org needs.

A first tiny but important security issue patch produced by Codeberg.org by has been accepted by the gitea maintainer team. A big Thank You!

Also we are thinking about the member poll and vote mechanism for the internal Codeberg e.V. member decision process, and think of a poll widget. If you fancy to help out in this project, please reach out.

The German tax authorities came back with regard to our request to officially recognize our non-profit status as entity that is allowed to send out tax-deduction forms for donations and contributions. In order to acknowledge our request, some minor bylaw changes have been requested. We will prepare this text (required in German as administrative language), and send it out as proposal via pull request. The voting process itself is yet to be decided, depending on whether the poll widget is ready in time or not. If the member vote agrees on those changes, and authorities request no further rework, non-profit-status would be valid for the next tax period, that is year 2020.

Long-term, it would be great to have informative English translations of all those changes and documents. If you would like to contribute, PRs in the org repository are very welcome!

The most important thing is to build momentum and continue to extend our community, to build and strengthen the social network of developers meeting on Codeberg.org.

Let's spread the word! We are looking forward to exciting times.

Yours truly,
Codeberg e.V.

