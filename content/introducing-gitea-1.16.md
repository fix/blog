Title: Introducing Gitea 1.16
Date: 2022-03-02
Category: Announcement
Authors: Otto Richter (Codeberg e.V.)

We finally deployed Gitea 1.16 a few days ago (on Friday, to be precise).
In case you missed it or didn't notice the changes,
I'll highlight what you *need to know* (not what you need to discover).

We had to wait for the Gitea 1.16 release longer than we expected,
and yet we were surprised by the "sudden" release of it.
The Gitea team is awesome, and we'd like to thank everyone who had their part in this release,
with all its features, bug fixes and enhancements.

The release and deployment of Gitea 1.16 was a little unfortunate.
Normally, we run careful offline migration and feature tests using snapshots
from the production system shortly after the first Release Candidate is up.
This time, we were busy by all kinds of other stuff,
and the initial 1.16 release was made before we had the chance to try it out.
A second release candidate was apparently skipped to fix a mistake and prevent docker users to inadvertently downgrade their versions.
Sadly, this initial release wasn't quite ready for deployment,
and it took two bugfix releases, some stressful nights of debugging and surely litres of coffee
until the webauthn migration was smooth enough for a deployment at Codeberg.
A big thanks goes to the Gitea team for their patience in ruling this out and collaborating with us for finding these bugs.
We hope looking for even better collaboration in the future and are looking into improving our snapshot/staging infrastructure for this task.


## What's included?

Talking about migrations and webauthn,
the Gitea team recommends to re-register legacy U2F keys with the new webauthn standard.
This is the way to get rid of the annoying reminder if you still log in with a former U2F key.

But let's talk about the features you *really wanted to see* :)

**Agit workflow support**: As of [#14295](https://github.com/go-gitea/gitea/pull/14295),
Gitea supports the agit workflow.
Basically, this allows you to create a PR directly from within Git, without even forking the repo.
After adding the upstream repo to your Git origins, you can just issue

~~~
git push -o topic="propose-feature-xy" -o title="My new PR title" -o description="Describe what I've done" upstream HEAD:refs/for/main
~~~

**Initial RSS feed implementation**: An often requested feature are RSS feeds.
Now, there is an initial reference implementation for users.
Check it out by appending `.rss` to your profile name.
We hope more people pick this up in the future, and provide RSS feeds where useful.

**More migration options**: We all know - vendor lock-ins are terrible.
Gitea is a very good example, with the development still being trapped at GitHub
due to the size of the project bursting the migration feature.
Yet, the Gitea migration is one of the most useful features of the service, and it was even improved recently.
Next to the already open-source GitBucket,
users of OneDev and CodeBase are now able to migrate to Gitea and into freedom.

**Issue comment history:** Editing of issue comments was possible for a long time.
Now, Gitea preserves the edit history, so you can track which info was added
(or which embarrassing typo hidden :D) in an issue.
Don't worry: If you really need to get rid of something, just click on the edit (which opens a pop-up),
and below the options (top-right) select "Delete from history".

**Revised team permissions**: The Gitea permission model was rather simple in the past.
Now, Gitea allows defining teams with finer grained permissions,
e.g. allow read access to code, but write access to issues while not granting access to the internal wiki at all.
We still need to reflect this in the documentation, helping hands warmly welcome:
[Codeberg/Documentation#204](https://codeberg.org/Codeberg/Documentation/issues/204)

Oh, and there's so much more.
You can find the whole changelog at [the Gitea blog](https://blog.gitea.io/2022/02/gitea-1.16.0-and-1.16.1-released/).
Again, thanks to everyone who contributed to this release, as maintainer, contributor, translator, issue reporter or reviewer.


## Codeberg-specific

At Codeberg, most changes were already incorporated during the previous release, or are still WIP.
Most people have noticed that we revamped our dark theme, finally providing codeberg-ish colour variants.
We were so free to upgrade everyone to the codeberg-dark theme.
If you preferred the arc-green theme, you can just switch it back in the user settings.
Oh, and in case you didn't notice: The `auto` themes now provide automatic toggling based on the browser settings.

## Contributing to Codeberg

Do you want to become part of our journey and help to build and maintain a true community software forge?
We are happy about every helping hand.
If you want to support Codeberg, feel free to check out the open community issues once the update is done (especially those that are
[looking for contribution](https://codeberg.org/Codeberg/Community/issues?state=open&labels=105)
and pick one up.
Also, we encourage everyone to pick up a good first issue upstream at Gitea to improve the base Codeberg relies on.
Again, we want to thank everyone who had their part in this Gitea release.

Also, feel free to drop us a donation that helps to maintain the infrastructure and provide new features.
And as always: Tell your friends!
