Title: Monthly Report January/February 2021
Date: 2021-03-17
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

We are hosting 9857 repositories, created and maintained by 8297 users. Compared to one month ago, this is an organic growth rate of +860 repositories (+9.6% month-over-month) and +657 users (+8.6%).

As we reported our annual numbers as part of our annual member assembly in February, the January newsletter had not been sent out. The January numbers would had been: We were hosting 8997 repositories, created and maintained by 7640 users. Compared to one month ago, this is an organic growth rate of +911 repositories (+11.3% month-over-month) and +706 users (+10.2%).

Our monthly expenses for domains and servers accumulated to 34.05 EUR in January, and 63.70 EUR in February. The machines have been upgraded and are running at about 29% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 106 members in total, these are 82 members with active voting rights and 24 supporting members.

Votes for presidium are in, and the new presidium had its constituting meeting. A new Vorstand has been appointed. Our annual meeting protocol is accessible by Codeberg e.V. members in the internal repo.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

