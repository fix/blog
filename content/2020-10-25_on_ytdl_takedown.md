Title: On the youtube-dl DMCA Takedown
Date: 2020-10-25
Category: Announcement
Authors: Codeberg e.V.


Last Friday (Oct 23, 2020), a [DMCA (Digital Millenium Copyright Act)](https://en.wikipedia.org/wiki/Digital_Millennium_Copyright_Act) takedown notice by the [RIAA (the Recording Industry Association of America)](https://en.wikipedia.org/wiki/Recording_Industry_Association_of_America) has effectively shut down development of youtube-dl, a tool to access content on video streaming platforms like Youtube. 

There seems to be a fundamental disagreement between the right holders and the community if this tool is legal or illegal.  We received a number of questions on social media how we would handle such a takedown request.

To answer this question, but also to assess potential risks and consequences for sustainability of Codeberg e.V. and Codeberg.org, and to outline viable options to go forward for all affected parties, we performed research and analysis of relevant rules and constraints. This post outlines our position and understanding of the issue. As usual, this is the result of careful research but nothing should be construed as legal advise. Our understanding, interpretation, and position may or may not change with incoming information.

## Legal background
In a very unusual turn RIAA justifies the takedown request with EU/German law and claims US analogy to EU rules, referencing a [case ruled by the Hamburg Regional Court](https://web.archive.org/web/20201023193007/https://github.com/github/dmca/blob/master/2020/10/2020-10-23-RIAA.md). This ruling decided that even very simple URL obfuscation schemes count as effective technical protection measure of digital content [full text here](https://web.archive.org/web/20201025145225/http://www.landesrecht-hamburg.de/jportal/portal/page/bsharprod.psml?showdoccase=1&doc.id=JURE180006255&st=ent). Basis of the decision was [§95a](https://web.archive.org/web/20200526021416/https://www.gesetze-im-internet.de/urhg/__95a.html), a paragraph introduced in the reform in German Copyright law in 2003.

This paragraph, paraphrased in plain English, essentially forbids all circumvention of technical protection measures of digital content, also the creation, trade, advertising, import, and distribution of such. 

Codeberg e.V. was founded in Germany and Codeberg.org is hosted in Germany, therefore we're tied to EU/German law. A DMCA takedown request by itself is not an issue for us.  But since the RIAA justifies their call with German law, we see a risk that Codeberg e.V. could become a target of similar requests.

## Viewpoints

Julia Reda, former member of the European Parliament specialized on copyright law reform and regulation of online platforms, via https://twitter.com/Senficon/status/1320442401194942470:

> In the EU, technologies that are marketed for, primarily designed for and have only limited use for other purposes than circumvention of technological protection measures for copyrighted content are illegal. As many YouTube videos are freely licensed, youtube-dl should be legal.

> However, using youtube-dl for the purpose of circumventing technological protection measures on copyrighted videos without permission of the rightholder is illegal.

> The legal basis is Article 6 of the InfoSoc directive. I don’t think the ads matter, only whether or not the rightholder of the original Youtube video has allowed copying (for example through a CC license) or the content is in the public domain.

The Electronic Frontier Foundation EFF posted via https://mastodon.social/@eff/105086779663199579:
> Youtube-dl is a legitimate tool with a world of a lawful uses. Demanding its removal from Github is a disappointing and counterproductive move by the RIAA.

## Our Position

First of all, Codeberg e.V. and the Codeberg platform was founded to support Free and Open Source (FOSS) code and content development.  We support the open source community and aim to make the Free and Open Source movement stronger.  We see ourselves on the good side and we do not support illegal activities on our platform.

However, if we for example host a legitimate open source tool and we would receive a similar notice, then we most likely would have to disable the repository until the matter is resolved by court ruling if such is fought through by the project owners.  This could include the code, issues, and documentation, which would immediately threaten/weaken the development community around that project.

We express severe doubts that the RIAA reference to the above mentioned Hamburg Court ruling is legitimate. As a large portion of content hosted on video streaming platforms is of educational nature created and posted by owners under permissive licenses such as Creative Commons with the clear intent to be freely shared for the common good, rendering a general-purpose tool like youtube-dl illegal is not justified.

We sincerely hope that organizations with established standing and funding resources much greater than ours, the Free Software Foundation FSF and the Electronic Frontier Foundation EFF just two among the most well-known, will join forces and prevent this from becoming a threatening precedence potentially eroding the most basic human rights on free access to education, knowledge, and communication.


--<br/>

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

