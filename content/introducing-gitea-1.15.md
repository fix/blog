Title: Introducing Gitea 1.15
Date: 2021-08-30
Category: Announcement
Authors: Otto Richter (Codeberg e.V.)

It's been exactly four months since the [deployment of Gitea 1.14](https://blog.codeberg.org/introducing-gitea-114.html). Today, we are pleased to announce the deployment of the Gitea 1.15 release. The database migration involved a downtime of about one minute.  
As always, the new version comes with a bunch of new features, bugfixes and other improvements. We are thankful to all the contributors of Gitea for their awesome work. We really appreciate using their software to build a true community-based software forge, and we are happy to support the work on the next release.

There were many small changes, and it's just impossible to cover all the changes. This blog post will guide you through the most important new features you'll love. If you want to know more, feel free to check out the full changelog at the [GitHub release page](https://github.com/go-gitea/gitea/releases/tag/v1.15.0) and the [Gitea blog](https://blog.gitea.io/2021/08/gitea-1.15.0-is-released/).

**Forge emojis:** Okay, probably not the most important feature, but ... you can now use `:codeberg:` (→ <img src="https://design.codeberg.org/logo-kit/icon.svg" style="height: 1em;">) along with some other forges to use their emoji in comments.  [*gitea#16296*](https://github.com/go-gitea/gitea/pull/16296)

**Tag protection:** If you are collaborating with some maintainers, but want to control tightly who is allowed to push new tags, especially for releases, you can now make use of tag protection. You can, for example, let everyone create release candidates, but limit creation of new releases to a subset of users to limit the danger of faulty releases. [*gitea#15629*](https://github.com/go-gitea/gitea/pull/15629)

**Clickable checkboxes:** Are you using issue checkboxes to handle your ToDo lists? You do no longer have to edit the markdown code. If you have write access to the content, you can now simply click into the checkbox to toggle them. [*gitea#15791*](https://github.com/go-gitea/gitea/pull/15791)

**Walking blames:** Although we hope that you don't use the blame feature to laugh at your contributors, it's a handy tool if you quickly need to find the origin of some changed line. Sometimes, you also want to know how it has been before or if there was something removed. The blame view now has a nice button to go visit the blame view from before a certain change. [*gitea#16259*](https://github.com/go-gitea/gitea/pull/16259)

**Review comment navigation:** Long pull requests, hundreds of commands? Quickly go through them with Previous and next buttons now! [*gitea#16273*](https://github.com/go-gitea/gitea/pull/16273)

**Diffing CSVs:** Ever tried to spot differences in long lines of CSVs? What was this field again? This got massively improved, you can now see a tabular view and per-cell diffs. Check out the screenshot in the pull request or try it yourself. [*gitea#14661*](https://github.com/go-gitea/gitea/pull/14661)

And many more changes you'll hopefully enjoy. They include for example the ability to migrate LFS objects with your repositories, easier comparing the commits between tags in your repos, and auto-removing unprotected branches after they are merged.

## Performance

Also, switching to the new Gitea version also brought some performance improvements. We saw many pull requests refactoring parts of the code, introducing caching and doing other improvements to the performance, cool thing! We noticed some decrease in CPU load, some parts of the web page speed up, and undeniable a decrease in memory usage:

![screenshot of server stats, memory usage suddenly drops from about 3GB to < 0.5 GB](/images/introducing-gitea-1.15_memory_usage.png)

Sadly, we also had some temporarily unavailability about 12 hours after deployment, maybe caused by a leak of file descriptors / pipes, and we put some countermeasures into place to keep the service running while investigating the issue. Further, we noticed some unresponsiveness in our storage (which appears to be unrelated as we discovered an occurence a few hours before the deployment). This lead to a few more short periods of unavailability (about 5 seconds unresponsiveness each), we are still investigating this issue, too.

## Codeberg specific changes

**Navbar:** Together with this release, we also deployed some codeberg-specific changes. Most prominent, you might have noticed another change to the navbar and the Codeberg dropdown. We now moved the help links from the right and integrated them into the Codeberg dropdown based on user feedback to our initial version.

**Repo banners:** Also, we are now experimenting with displaying Codeberg banneres to your repos. This is currently a proof-of-concept and must be considered a WIP. The goal is to nudge users and catch their attention when they are hosting huge (often private) repos, consuming many resources, or when they miss a licence. Instead of handling every violation with an abuse email, we now want to easily catch the users attention prior to acting, also to save us some work. We hope it works out - for sure, we'll keep working on this experiment between the Gitea releases.

![Screenshot of a banner, reading: 
Your private repo uses up an insanely large amount of disk storage, while all content should ideally be public and licensed under an OSI/FSF-approved Free Software licence.
Please refer to our ToS and the FAQ about software licenses and private repositories
Thank you for considering to release this repo to the public or reducing your required disk space for this repo.](/images/introducing-gitea-1.15_banner_example.png)

If you want to stay up to date with our work on Codeberg, also make sure to read the [monthly letters](https://blog.codeberg.org/category/monthly-letter.html).

## Contributing to Codeberg

Do you want to become part of our journey and help to build and maintain a true community software forge? We are happy about every helping hand.
If you want to support Codeberg, feel free to check out the open community issues once the update is done (especially those that are [looking for contribution](https://codeberg.org/Codeberg/Community/issues?state=open&labels=105) and pick one up. Also, we encourage everyone to pick up a good first issue upstream at Gitea to improve the base Codeberg relies on. Again, we want to thank everyone who had their part in this Gitea release.

Also, feel free to drop us a donation that helps to maintain the infrastructure and provide new features. And as always: Tell your friends!
