Title: Let's bring Codeberg to the cloud
Date: 2022-04-01
Category: Announcement
Authors: Knut (Codeberg e.V.)


**In case you read this later and didn't yet spot the date: This was an April's Fool for 2022, please don't take it too serious.  
TL;DR: Our own hardware journey reaches the next level - we are finally moving fully to the cloud.**

About a year ago, we decided we'd want to go next steps for the hosting of your user data.
Running on other people's computers seemed very expensive long-term,
especially for large storage sizes.
We calculated that we would quickly save storage costs when buying own hardware, and so we did.

The journey was everything but smooth.
At first, the decision-making progress caused a lot of headache.
Should we opt for one server, or two for redundancy? Where to host it?
Then, global delivery issues caused delays,
and when we finally received our server, it took us much longer than expected to move it into a datacenter.

Now, it's humming there, building your projects through [WoodpeckerCI](https://mastodon.technology/@WoodpeckerCI/108056475984630823),
and serving your Codeberg Pages.
Still, we feel that the situation is still suboptimal.


## The issues in our datacenter

Although the datacenter is running on renewable energy,
we don't have a way to connect our own solar panels directly to our server.
Also, while the datacenter is only using air conditioning in summer,
we'd love to completely rely on natural cooling in the future.

More issues become obvious when considering the fixed location of the server.
While most of our team is currently based in Berlin and on-site operation was easily possible in the past,
we can't guarantee this forever, and wish we had more flexibility in case the team members were moving somewhere else.
Also, we'd love to involve more people from around the world.

Last but not least, when the uplink of our server failed a few weeks ago,
we'd have appreciated if it was easier to move the server somewhere else.


## The solution is called 'cloud'

We investigated this issue a lot, and learned that many large platforms have found their solution in the 'cloud'.
So after some thorough research and calculation,
we determined that the only viable strategy is to lift off our servers and bring them to our own cloud.

We just signed a contract with a zeppelin manufacturer that wishes to remain anonymous for the time being,
and we are very proud about this huge milestone, looking forward to the take off date at [1st of April 2023](https://en.wikipedia.org/wiki/April_Fools%27_Day).
We are using the remaining full year for containerizing all our services and applications,
in order to easily hang the systems below the airship.


## Flexibility is key

Bringing our systems to the cloud grants us a lot of flexibility and solves some other issues as side-effect.
Now, we can easily ship the whole application to the nearest sysadmin in case of failures.
Remote-debugging will no longer be necessary.
We can even drop out our lights-out-management, as long as we always travel in the sunshine.

Since the zeppelin only uses natural buoyancy, we can spend nearly all energy on powering the systems itself.
Therefore, we are installing solar panels on the upside,
and using battery storage for the times the winds don't keep us in the sunlight.
Further, we can finally abandon air conditioning,
because the air circulation at low temperature is enough in this windy height.

Additionally, we also solve the issue of global network latency:
Because the airship is moving around the earth,
we do no longer privilege users in Central Europe which are near our existing setup.
Instead, latency now randomly fluctuates for all users on the globe with our airship travelling around.

We only rely on prevailing winds to move our system, so the movement isn't fully deterministic.
The first anticipated routes are from Berlin via Munich to Marseille, then east (possibly up to Japan, but we can't know for sure),
and then we'll let ourselves be surprised.

Thank you for being in this journey together.  
- the Codeberg team

Oh, and don't forget to chip in some money, as Codeberg is powered by donations from people like you!

Oh, and in case we don't receive enough money for this extraordinary plan,
we'll instead use your donations to invest into hardware redundancy,
so that we can finally move our production Gitea instance to own hardware, too.
