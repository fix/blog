Title: First Codeberg Hackathon in April 2022
Date: 2022-03-29
Category: Events
Authors: Otto Richter (Codeberg e.V.)


*TL;DR: We invite you to take part in the Codeberg Hackathon on the weekend from April 8th to April 10th 2022 and help improving the ecosystem around Codeberg.*

We are taking collaborative notes in this [HedgeDoc](https://pad.ccc-p.org/C3bk_zFTSYarbaOQPZeJLg?both).  
Join the [Senfcall (BigBlueButton meeting)](https://lecture.senfcall.de/ott-qfr-xes-p20)
(if no one is online, return at the scheduled times below).

In order to make Gitea the most awesome and community-maintained software forge,
as well as Codeberg an instance that makes everyone happy,
we need your helping hand.
And because work is often more fun when done together,
and it's probably nice to get to know other Codeberg users,
we are now planning our first hackathon:
A weekend to get our hands on the keyboards together and work on our shared home for Free Software.

I had such an event in mind for quite a while, and I'm very pleased to announce we'll have it on the second weekend in April:
From Friday 8th to Sunday 10th of April 2022.
If you ever wanted to change this and that about Codeberg and Gitea, this is your best chance to get it done!

We will be meeting in Senfcall, a BigBlueButton instance also community-managed as non-profit.
The invite link is: <https://lecture.senfcall.de/ott-qfr-xes-p20> (double-check this post before joining in case of updated information).


## The Roadmap

The short plan for this weekend: Do whatever you want, seek help and inspiration by others.
Codeberg admins and maintainers will be there to answer your questions.

If you don't yet have a project idea on your own, or you would like to help us with the most important issues on our side,
we'll be presenting some project ideas on Friday evening and Saturday morning.
As soon as people are interested and group around an idea, you can get started together,
bootstrapping a new feature or side-project, and we'll happily stand by your side to make sure your experience is straightforward.

Some ideas might be:

- doing patches and fixes to Gitea / Woodpecker CI
- continuing our work on [Codeberg Pages](https://codeberg.org/Codeberg/pages-server) or the Codeberg [Moderation Tool](https://codeberg.org/Codeberg/moderation)
- revisiting the registration server and adding some basic member management features
- discussing server setups and configuration
- discussing our idea of a [Captcha Service](https://codeberg.org/Codeberg-Infrastructure/CaptchaService), possibly starting a prototype
- [creating Gitea issues and patches via mail](https://codeberg.org/Codeberg/Community/issues/526)
- building a mirror service that allows Codeberg users to [contribute to proprietary forges](https://codeberg.org/Codeberg/Community/issues/607)
- Use an open-source blog theme [Codeberg/blog#15](https://codeberg.org/Codeberg/blog/issues/15)
- Codeberg virtual space [Codeberg/Community#586](https://codeberg.org/Codeberg/Community/issues/586)
- Bring Codeberg to the Wikipedia
- any other issue you encounter, especially those labelled as ["contribution welcome"](https://codeberg.org/Codeberg/Community/issues?state=open&labels=105)


## The schedule

This event is community-driven and powered by people like you,
thus this schedule is only an estimate: Times might differ depending on personal circumstances.
Yet, this is what we are currently aiming for (and we'll update it here in the blog post if necessary).
**If not stated otherwise, all times are UTC**.

**Friday, April 8th 2022:**

- 18.00 UTC (20.00 CEST): Opening, presentation of ideas and brainstorming
- ~ 19.30 UTC (21.30 CEST): Get-together and open hacking: Chill the evening and talk to like-minded people, or already get started with your project (open end)

**Saturday, April 9th 2022:**

- 09.00 UTC (11.00 CEST): (Again) presentation of ideas from Friday, forming of workgroups
- 09.30 UTC (11.30 CEST): Open Hacking on your projects. Codeberg admins and maintainers will try to be around to answer your questions.
- 12.00 UTC (14.00 CEST): Short evaluation of your progress and the format, onboarding for new people (or break, if you prefer)
- 13.00 UTC (15.00 CEST): Open Hacking part #2.
- 18.00 UTC (20.00 CEST): Evaluation of day one, agendasetting for Sunday
- 18.30 UTC (20.30 CEST): Get-together and chill (or further hacking if you prefer, open end)

**Sunday, April 10th 2022:**

- 09.00 UTC (11.00 CEST): Open hacking on your projects.
- 12.00 UTC (14.00 CEST): Short evaluation of your progress or break, onboarding for new people
- 13.00 UTC (15.00 CEST): Open hacking part #2, Codeberg admins and maintainers will try to learn about your progress and discuss how to eventually continue the project after the hackathon
- 18.00 UTC (20.00 CEST): Presentation of results, evaluation of the event, discussing further plans
- 19.00 UTC (21.00 CEST): Get-together and chill (or further hacking if you prefer, open end)

Thank you very much!


## Questions and answers

### Do I need to register to participate?

No, just join the room and talk. If you want to let us know we can count you in or you have project ideas,
feel free to check by in the ["Contributing to Codeberg" Matrix Channel](https://matrix.to/#/!wIJGSzCYITbuEkORkq:matrix.tu-berlin.de).

### Are there requirements for helping out? Do I have to know (...how to code / ...a programming language / ...about some codebase)?

No, we welcome everyone, even newcomers and people that want to learn something.
While we can't guarantee that someone is around to teach you something,
we'll do our best to make sure you learn something if you are interested.

In any case, we'll find something to do for everyone - so if you can spare the time and want to help out, just join us!

### I can't attend the full weekend, is this a problem?

No, please join anyway. You do not have to attend the full time.
We are doing two presentations of projects, to grant more people the chance to get in.
We are also doing some scheduled onboarding, if you want to join later.
As long as someone from the team is online, you can join any time and leave any time,
but if you took the responsibility for a project, we'd be happy to stay in touch with you about the future.

### What if I can't finish my idea on the weekend?

That's not a problem, in fact to be expected for everything that goes beyond a tiny fix.
If you are working in a work group, we'll try to discuss the future of the project on Sunday.
If you have a tiny project on your own, please get and stay in touch with us and we'll make a plan for how to move on.
In any case, we appreciate your contribution, and we appreciate even more if you commit to finishing your started work afterwards.

### I just joined the meeting, but no one is online.

We can't guarantee to always have people around, but we'll surely be there during the announced onboarding times.
If you can, please wait a moment to see if someone joins you, or write a short message in the Matrix group.

### Your times absolutely don't work for me

If you live in a part of the world (or have a completely different day/night rhythm) that doesn't align well with our schedule,
we are very sorry. Please give us feedback in that case.
We are looking forward to doing more events of that kind in the future,
and we'd love to adapt to the wishes of our users and members.
