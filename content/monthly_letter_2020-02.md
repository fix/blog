Title: Monthly Report January 2020
Date: 2020-02-27
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

It is almost mid-month, time for updates.

We created a group "Members" on codeberg.org, to facilitate discussion between Codeberg e.V. members. For privacy reasons we add members on request. If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

The protocol for our annual member assembly on January 19th in Berlin, is online at https://codeberg.org/Codeberg/Protokoll-Mitgliederversammlungen/src/branch/master/2020-01-19_Protokoll.md (sorry, German language text, as we have to file this with local authorities. Automatic translation tools do a pretty good job on legal texts, tho, and we will be happy to answer any question to clarify uncertainties or doubts!). It was a nice evening, a great thank you for everybody who joined!

We discussed a few issues we want to vote on, we will soon send out voting tokens via email to all members, and organize the online-poll for each of them.

Also, we need to poll for the election of our next cash auditor. We currently have exactly one candidate, our early supporter Henning Jacobs. In his own words:

> """I'm passionate about Open Source / Free Software and like to solve problems with Python. I deeply believe that Free Software is a key ingredient to unlock humankind's potential and that the platform for code hosting should be Free Software, too. I'm trying to gradually move my Open Source projects from the proprietary platform GitHub to codeberg.org and I actively promote its use on social media. I would be happy to support Codeberg e.V. as a cash auditor. I currently work at Zalando in Berlin as a Principal Engineer. You can learn more on my personal blog: https://srcco.de/."""

Please watch your inbox!

Codeberg e.V. has 51 members (41 active, 10 supporting). We are hosting 2253 repositories, created and maintained by 1779 users. Compared to one month ago, this is an organic growth of rate of +276 repositories (+14% month-over-month). We have deleted almost 3000 automatically created accounts which were never activated. After some changes to the account creation process, we now experience very few attempts to create new abusive user accounts.

The machines are runnnig at about 48% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggested that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers. As we have now passed this threshold, we can now re-run initial calculations and check options for a logical next step.

Your Codeberg e.V.

---
https://codeberg.org

Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929.
 
