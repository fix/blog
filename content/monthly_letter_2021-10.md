Title: Monthly Report September 2021
Date: 2021-10-27
Category: Monthly Letter
Authors: Otto Richter (Codeberg e.V.)

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!


We are hosting 16791 repositories, created and maintained by 14574 users. Compared to one month ago, this is an organic growth rate of +1196 repositories (+7.7% month-over-month) and +953 users (+7%).


**CI**: After some team members invested a lot of work into Woodpecker, we dared to launch a closed-alpha test with our new CI on the bare-metal server. This ate up most of our time this month. There is still a long way to go for finer quota tuning and abuse prevention (this is the main reason for granting access on request). Also check out the social media announcement on [Mastodon](https://mastodon.technology/@codeberg/106931569806712591).

**OAuth linking**: Migration from other services to Gitea is already a nice and convenient feature for a long time. However, previous comments were not assigned to the original author. Now, you can link your GitHub and GitLab.com profiles to Codeberg and migrated content links to your Codeberg profile and login to Codeberg using those providers once linked. This feature is of course fully optional, please be aware that the third-party providers will learn about your linking and login attempts if you use the quick login feature. Here is how it works:

 - Register a Codeberg account as usual (not necessary as you have it)
 - click on "Link Account" at the sign in page
 - choose a third-party provider and authorize Codeberg (you will share data with them)
 - migrated content now links to your profile
 - you can also use these to login to Codeberg if this is more convenient for you, but they'll learn about your logins then.


The machines are running at about 46% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 147 members in total, these are 110 members with active voting rights and 36 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 

